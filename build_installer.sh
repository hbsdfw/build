#!/bin/sh

. config/common.subr
. config/common_start.subr

set -x

# extract the version number from the symlink
HS_VERSION=$(readlink ${HS_REPO}/FreeBSD:14:amd64/latest)

# src/release/Makefile target
IMAGE_TARGET=disc1.iso
IMAGE_NAME=disc1.iso
IMAGE_EXT=.iso
COMPRESS_IMAGE=0

# location of poudriere pkg build output
HS_PKG_REPODIR="`realpath ${HS_WORK_DIR}`/poudriere/base/data/packages/${POUDRIERE_JAIL}-${POUDRIERE_PORTS}"

# location of OS image files
RELEASE_DIR=${MAKEOBJDIRPREFIX}/${HBSD_TREE}/amd64.amd64/release/

# list of ports to install in the base hbsdfw
PORTS_FILE="`realpath ${HS_CONFIG_DIR}/ports.conf`"

#
# create list of ports that will be installed in the base hbsdfw
#

create_ports_list() {
	tar -xOf ${HS_PKG_REPODIR}/packagesite.txz packagesite.yaml | jq -r .path
}


#
# build the Makefile target disc1.iso as an installable disc image.
#
build_installer()
{
    name="hbsdfw_installer_${1}_${HS_VERSION}-${date}${IMAGE_SUFFIX}${IMAGE_EXT}"
    shift
    opts="$@"

    sudo chflags -R noschg ${RELEASE_DIR}
    sudo rm -rf ${RELEASE_DIR}
    cd ${HBSD_TREE}/release && sudo -E make ${IMAGE_TARGET} WITH_PKGBASE=y REPODIR=${HS_REPO} \
				    VENDOR_REPODIR=${HS_PKG_REPODIR} \
                    VENDORNAME=hbsdfw \
				    PORTSDIR=${PORTSDIR} \
				    NODOC= ${opts}
    mkdir -p ${IMAGE_DIR}/
    cp ${MAKEOBJDIRPREFIX}/${HBSD_TREE}/amd64.amd64/release/${IMAGE_NAME} ${IMAGE_DIR}/${name}
    cat ${IMAGE_DIR}/${name} | sha256 > ${IMAGE_DIR}/${name}.sha256

    if [ ${COMPRESS_IMAGE} -gt 0 ]; then
	bzip2 -k -v -9 ${IMAGE_DIR}/${name}
	cat ${IMAGE_DIR}/${name}.bz2 | sha256 > ${IMAGE_DIR}/${name}.bz2.sha256
    fi

    echo "[*] ISO is at ${IMAGE_DIR}/${name}"
}

# make log directory
mkdir -p ${LOGS_DIR}

# check that the ports list is not empty
export VENDOR_PORTS_LIST=`create_ports_list`
if [ $? -ne 0 -o -z "${VENDOR_PORTS_LIST}" ]; then
	echo "The ports list is empty."
	exit 1
fi

# build installer images
#build_installer serial "WITH_SERIAL=yes" 2>&1 | tee -a ${LOGS_DIR}/build_installer.log
build_installer vga 2>&1 | tee -a ${LOGS_DIR}/build_installer.log
