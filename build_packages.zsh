#!/usr/local/bin/zsh -x

CLEAN=n
UPDATE_PORTS=n
NEW_PORTS=n
OVERLAY_PORTS=""
EXTRA_PORTS=""

. config/common.subr

# Either empty or -c for a clean bulk
PORT_BULK=

#PORTS_FILE="`realpath PORTS_LIST`"

usage()
{
    cat <<EOF
Usage: `basename $0` [options]
Options:
	-b	Git branch for the jail (default: dynfi-13-stable)
	-c	Recreate the jail
	-e	Path to extra ports file (default: "")
	-n	Create new poudriere ports tree
	-o	Ports tree to use for the overlays (default: dynfi-overlay)
	-p	Git branch for the portstree (default: default)
	-u	Update ports tree
EOF
    exit 1
}

error()
{
    echo "$1"
    exit 1
}

build_new()
{
	## create new poudriere ports tree
    if [ "${NEW_PORTS}" = "y" ]; then
        echo ""
        echo "Building new poudriere ports tree"
        echo ""
	    ./poudriere.sh ports -c -p ${POUDRIERE_PORTS} -B ${PORT_BRANCH} -U https://git.hardenedbsd.org/hbsdfw/ports.git
    fi
}

build_jail()
{
    # Build poudriere jail and poudriere package
    # We need to always rebuild the jail so the userland/kernelland used for compiling
    # packages is the same as the one we will run
    if [ "${CLEAN}" = "y" ]; then
	echo ""
	echo "Building the poudriere jail"
	echo ""
	yes | sudo -E poudriere jail -d -j ${POUDRIERE_JAIL}
	#sudo -E poudriere jail -c -j ${FBSD_BRANCH} -m git -U http://192.168.99.219/gitmob/freebsd/ -v ${FBSD_BRANCH} || error "Poudriere: jail compile failed"
	./poudriere.sh jail -c -j ${POUDRIERE_JAIL} -v ${HBSD_BRANCH} -m git+https -U https://git.hardenedbsd.org/hbsdfw/hardenedbsd.git
    fi
}

build_packages()
{
    local cleanarg
    if [ "${UPDATE_PORTS}" = "y" ]; then
	echo ""
	echo "Updating the ports tree"
	echo ""
	./poudriere.sh ports -u -p ${POUDRIERE_PORTS} || error "Poudriere: ports update failed"
    fi

    echo ""
    echo "Building the packages using branch ${PORT_BRANCH}"
    echo ""
    ./poudriere.sh bulk \
	    ${BULK_CLEAN} \
	    -j ${POUDRIERE_JAIL} \
	    -p ${POUDRIERE_PORTS} \
	    -f ${HS_WORK_DIR}/${HS_CONFIG_DIR}/ports.conf \
	    ${=OVERLAY_PORTS} \
	    ${=EXTRA_PORTS} \
	    ${cleanarg}
}

while [ $# -ne 0 ]; do
    case "$1" in
	-e)
		shift
		EXTRA_PORTS=$1
		;;
	-n)
        NEW_PORTS=y
	    ;;
	-c)
	    CLEAN=y
	    BULK_CLEAN=-c
	    ;;
	-C)
	    BULK_CLEAN=-c
	    ;;
	-b)
	    shift
	    HBSD_BRANCH=$1
	    shift
	    ;;
	-o)
	    shift
	    OVERLAY_PORTS="$1"
	    shift
	    ;;
	-p)
	    shift
	    PORT_BRANCH=$1
	    shift
	    ;;
        -u)
	    UPDATE_PORTS=y
	    ;;
	-h)
	    usage
	    ;;
    esac
    shift
done

. config/common_start.subr

mkdir -p ${LOGS_DIR}
build_new 2>&1 | tee -a ${LOGS_DIR}/build_new_ports.log
build_jail 2>&1 | tee -a ${LOGS_DIR}/build_jail.log
build_packages 2>&1 | tee -a ${LOGS_DIR}build_packages.log
